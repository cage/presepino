/*
  presepino: show LED patterns
  (c) 2018  cage

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.
  If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

 */

DS3231_Simple Clock;

void clock_setup() {
  Clock.begin();
}

void clock_set_hour (unsigned char h) {
   DateTime newHour;
   newHour.Hour = h;
   Clock.write(newHour);
}

uint8_t clock_get_hour () {
#ifndef FIXED_TIME
  return Clock.read().Hour;
#else
  return 22;
#endif
}

uint8_t clock_get_minute () {
#ifndef FIXED_TIME
 return Clock.read().Minute;
#else
 return 0;
#endif
}

unsigned char clock_get_temperature () {
  return Clock.getTemperature();
}
