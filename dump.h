/*
  presepino: show LED patterns
  (c) 2018  cage

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.
  If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

 */

#define OFFSET_ANIM_NUM  EEPROM_OFFSET_DUMP

#define OFFSET_LED_NUM   (EEPROM_OFFSET_DUMP + 1)

#define OFFSET_ANIM_LIST (EEPROM_OFFSET_DUMP + 2)

#define ANIM_TYPE_FADE   0

#define ANIM_TYPE_PULSE  1

#define INFO_TEMPLATE    "%i,%i,%i,%i"

char info[24];

void dump_write_at (int address, uint8_t value) {
  eeprom_write(address, value);
}

uint8_t dump_read_at (int address) {
  assert(address < EEPROM_SIZE);
  return EEPROM.read(address);
}

int dump_read_anim_num () {
  return EEPROM.read(OFFSET_ANIM_NUM);
}

int dump_read_led_num () {
  return EEPROM.read(OFFSET_LED_NUM);
}

int dump_length () {
  return 2 + (dump_read_anim_num() * (dump_read_led_num() * 3 + 2));
}

int dump_anim_length () {
  return (dump_read_led_num() * 3 + 2);
}

int dump_offset_anim (int idx) {
  return EEPROM_OFFSET_DUMP + 2 + (idx *  dump_anim_length());
}

int dump_offset_type (int idx) {
  return 1 + dump_offset_anim(idx);
}

int dump_offset_data (int idx) {
  return 1 + dump_offset_type(idx);
}

int dump_get_hour_at (int idx) {
  assert(idx < dump_read_anim_num());
  int offset = dump_offset_anim(idx);
  return dump_read_at(offset);
}

int dump_get_type_at (int idx) {
  assert(idx < dump_read_anim_num());
  int offset = dump_offset_type(idx);
  return dump_read_at(offset);
}

bool_t dump_type_at_fade_p (int idx) {
  return dump_get_type_at(idx) == ANIM_TYPE_FADE;
}

void dump_get_color_at (int idx_state, int idx_color, uint8_t* h, uint8_t* s, uint8_t* v) {
  assert(idx_state < dump_read_anim_num());
  assert(idx_color < dump_read_led_num());
  int offset  = 1 + dump_offset_type(idx_state);
  offset     += idx_color * 3;
  *h = dump_read_at(offset);
  *s = dump_read_at(offset + 1);
  *v = dump_read_at(offset + 2);
}

/**
  if the  chosen left limit is  not a fading animation  we go backward
  until the first non fading animation of that batch is found.
 **/

void dump_fix_left_limit (int* left_limit) {
  if (dump_type_at_fade_p(*left_limit)){
    return;
  } else {
    /*
       no  fading type,  go backward  until  one is  found (ore  start of  the
       manimation is reached)
     */
    while (((*left_limit) > 0) &&
           !dump_type_at_fade_p(*left_limit)){
      *left_limit = *left_limit - 1;
    }

    if (*left_limit > 0) {
      *left_limit = *left_limit + 1;
    }
  }
}

/**
  if the  chosen left limit is  not a fading animation  we go forward from here
  until the last non fading animation ofthat batch is found.
 **/

void dump_fix_right_limit (int* left_limit, int* right_limit) {
  if (!dump_type_at_fade_p(*left_limit)){
    *right_limit = *left_limit +1;
    while (((*right_limit) < (dump_read_anim_num() - 1)) &&
           !dump_type_at_fade_p(*right_limit)){
      *right_limit = *right_limit + 1;
    }
  }
}

void dump_get_anim_limits(uint8_t hour, int* left_limit, int* right_limit) {
  int i = 0;
  for (i = 0; i <  dump_read_anim_num(); i++){
    int current_anim_h = dump_get_hour_at(i);
    if(current_anim_h > hour){
      *left_limit  = rotate_left(i, 1, dump_read_anim_num());
      *right_limit = i;
      break;
    }
  }

  if (i == (dump_read_anim_num())) { // hour is greater than the last hour in animation
    *left_limit  = dump_read_anim_num() - 1;
    *right_limit = 0;
  }

  dump_fix_left_limit  (left_limit);
  dump_fix_right_limit (left_limit, right_limit);
}


void dump_get_anim_limits(int* left_limit, int* right_limit) {
  dump_get_anim_limits(clock_get_hour(), left_limit, right_limit);
}

char* dump_info () {
  sprintf(info, INFO_TEMPLATE,
          dump_read_anim_num(),
          dump_read_led_num(),
          dump_length(),
          dump_anim_length());
  return info;
}
