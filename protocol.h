/*
  presepino: show LED patterns
  (c) 2018  cage

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.
  If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

 */

#define OTR                     '#'

#define HELLO                   'H'

#define SET_CLOCK               'C'

#define ALLOCATE_MEMORY         'A'

#define STORE_DUMP              'D'

#define TEST_COLOR              'T'

#define ERROR_REPLY             'E'

#define OK_REPLY                'K'

#define EXTENSION_REPLY         'X'

#define EXTENSION               'X'

#define EXT_TEMPERATURE         'T'

#define ERR_NO_MEMORY           1

#define ERR_INVALID_COLOR       2

#define ERR_TOO_MANY_LEDS       3

#define ERR_CLOCK_HOUR_INVALID  4

#define CMD_TERMINATOR          '\n'

#define BUFFER_SIZE            10

#define READ_RETRY_NUM         10

unsigned char read_buffer [BUFFER_SIZE] = {0xAB};

void read_line () {
  Serial.readBytesUntil(CMD_TERMINATOR, (char*)read_buffer, BUFFER_SIZE);
}

int read_byte (int timeout) {
  delay(10);
  if (timeout < 0) {
    return -1;
  } else if (Serial.available() > 0) {
    int res = Serial.read();
    return res;
  } else {
    timeout--;
    read_byte(timeout);
  }
  return -1; // never reached
}

int read_byte () {
  return read_byte(READ_RETRY_NUM);
}

char* read_bytes (int num) {
  char* res = new char[num];
  Serial.readBytes(res, num);
  return res;
}


void send_end_cmd () {
  Serial.write(CMD_TERMINATOR);
}

void send_ok () {
  Serial.write(OK_REPLY);
  send_end_cmd();
}

void send_error (int code) {
  Serial.write(ERROR_REPLY);
  Serial.write(code);
  send_end_cmd();
}

void send_comment (unsigned char v) {
  Serial.write(OTR);
  Serial.write(v);
  send_end_cmd();
}

void dump_read_buffer () {
  for (int i = 0; i < BUFFER_SIZE; i++){
      Serial.println(read_buffer[i]);
  }
}

void set_clock () {
  unsigned char h = read_buffer[0];
  if (h < 23 && h >= 0) {
    clock_set_hour(h);
    send_ok();
  } else {
    send_error(ERR_CLOCK_HOUR_INVALID);
  }
}

void test_color_on_leds () {
  unsigned char h = read_byte();
  unsigned char s = read_byte();
  unsigned char v = read_byte();
  unsigned char c = read_byte(); // unused
  led_preview_hsv(h, s, v);
  led_show();
  send_ok();
}

void send_ext_error () {
  Serial.write(EXTENSION_REPLY);
  Serial.write(ERROR_REPLY);
  Serial.write(1);
  send_end_cmd();
}

void send_ext_temp () {
  Serial.write(EXTENSION_REPLY);
  Serial.write(clock_get_temperature());
  send_end_cmd();
}

void manage_extension () {
  unsigned char prefix = read_buffer[0];

  switch (prefix) {
  case EXT_TEMPERATURE:
    {
      send_ext_temp();
      break;
    }
  default:
    {

    }
  }
}

int make_16bit_num (unsigned char a, unsigned char b) {
  int res = a << 8;
  return res | b;
}

void allocate_memory () {
  unsigned char a = read_byte();
  unsigned char b = read_byte();
  unsigned char c = read_byte(); // unused
  int mem_size    = make_16bit_num(a, b);
#ifdef DEBUG
  send_comment(a);
  send_comment(b);
  char tmp[6] = {0};
  sprintf(tmp, "%i", mem_size);
  for(int i=0; i < 6; i++){
    send_comment(tmp[i]);
  }
#endif
  if(mem_size < EEPROM_SIZE) {
    alloc_anim_size     = mem_size;
    dump_current_offset = EEPROM_OFFSET_DUMP;
    send_ok();
  } else {
    send_error(ERR_NO_MEMORY);
  }
}

void store_dump_eeprom () {
  int a  = read_byte();
  int lf = read_byte();
  if (a >= 0) {
#ifdef DEBUG
    Serial.print(F("dump #"));
    Serial.print(dump_current_offset);
    Serial.print(F("/"));
    Serial.print(alloc_anim_size);
    Serial.print(" ");
    Serial.println(a);
#endif
    dump_write_at(dump_current_offset, a);
  } else {
#ifdef DEBUG
    Serial.print(F("dump #"));
    Serial.print(dump_current_offset);
    Serial.print(F("/"));
    Serial.print(alloc_anim_size);
    Serial.print(F("E"));
    Serial.println(a);
#endif
  }
  send_ok();
}

void store_dump() {
  store_dump_eeprom();
}

void recv_command () {
  int prefix = read_byte();

  switch (prefix) {
  case HELLO:
    {
      read_line(); // discard
      send_ok();
      break;
    }
  case SET_CLOCK:
    {
      read_line();
      set_clock();
      break;
    }
  case TEST_COLOR:
    {
      test_color_on_leds();
      break;
    }
  case ALLOCATE_MEMORY:
    {
      allocate_memory();
      break;
    }
  case STORE_DUMP:
    {
      eeprom_initialize();
      store_dump();
      dump_current_offset++;
      break;
    }
  case EXTENSION:
    {
      read_line();
      manage_extension();
      break;
    }
  }
}
