/*
  presepino: show LED patterns
  (c) 2018  cage

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.
  If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

 */

#define KEYPAD_ROWS  4

#define KEYPAD_COLS  3

char keys[KEYPAD_ROWS][KEYPAD_COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  { 0, '0', 0 }
};

uint8_t row_pins[KEYPAD_ROWS]    = {41, 43, 45, 47};

uint8_t column_pins[KEYPAD_COLS] = {49, 51, 53};

Keypad  the_keypad               = Keypad(makeKeymap(keys), row_pins, column_pins,
                                          KEYPAD_ROWS, KEYPAD_COLS );

char keypad_get_key () {
  return the_keypad.getKey();
}

char keypad_block_key_pressed(){
  char c = keypad_get_key();
  while (!c) {
    c = keypad_get_key();
  }
  return c;
}
