/*
  presepino: show LED patterns
  (c) 2018  cage

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.
  If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

 */

#define VERSION_MAJOR                 0

#define VERSION_MINOR                 0

#define VERSION_REVISION              1

#define EEPROM_SIZE                4096

#define EEPROM_OFFSET_DUMP            3

#define LED_NUM                      60

#define LED_DATA_PIN                  2

#define LED_TEMPERATURE          Candle

#define LED_CORRECTION  TypicalLEDStrip

#define LED_MAX_BRIGHTENSS           42

#define FRAMES_PER_SECOND           120

#define TRUE                          1

#define FALSE                         0

typedef int bool_t;

// define if a keypad is used to setup the clock at power on

//#define USE_KEYPAD_FOR_CLOCK_SET

// debug stuff

//#define  DEBUG

//#define  FIXED_TIME

//#define DUMP_EEPROM
