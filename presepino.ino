/*
  presepino: show LED patterns
  (c) 2018  cage

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.
  If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

 */

#include <inttypes.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#include <SPI.h>

#include <Wire.h>

#include <EEPROM.h>

#include <avr/pgmspace.h>

#include <DS3231_Simple.h>

#include <FastLED.h>


#include <Keypad.h>


int alloc_anim_size     = 0;

unsigned int dt         = 0;

#include "constants.h"

int dump_current_offset = EEPROM_OFFSET_DUMP;

bool_t run_animation_p = TRUE;

#include "utils.h"

#include "clock.h"

#include "dump.h"

#include "led.h"

#include "protocol.h"

#include "keypad.h"

#ifdef USE_KEYPAD_FOR_CLOCK_SET

void wait_keypad_visual_feedback(uint8_t hue) {
  led_preview_hsv(hue, 255, 255);
  led_display();
}

int wait_keypad (uint8_t hue) {
  wait_keypad_visual_feedback(hue);
  return keypad_block_key_pressed() - 48;
}

void set_clock_keypad (){
  wait_keypad_visual_feedback(0); // red

  int dec  = wait_keypad(41);       // yellow
  int unit = wait_keypad(74);       // green
  int h    = dec * 10 + unit;

  if (h >= 0 && h < 24){
    clock_set_hour((unsigned char)h);
  } else {
    set_clock_keypad();
  }

}

#endif

void dump_eeprom() {
  delay(2000);
  for (int i = 0; i< EEPROM_SIZE; i++){
    Serial.print(i);
    Serial.print(F(": "));
    Serial.println(EEPROM.read(i));
  }
}

void setup() {
  Serial.begin(9600);
  clock_setup();
  led_setup();

#ifdef USE_KEYPAD_FOR_CLOCK_SET
set_clock_keypad();
#endif

#ifdef DUMP_EEPROM
  Serial.println(dump_info());
  Serial.println(dump_get_hour_at(0));
  Serial.println(dump_get_hour_at(1));
  Serial.println(dump_get_hour_at(2));
  Serial.println(dump_get_hour_at(3));
  Serial.println(dump_get_hour_at(4));
  Serial.println(dump_get_hour_at(5));
  Serial.println(dump_get_hour_at(6));
  Serial.println(dump_get_hour_at(7));
  Serial.println(dump_get_type_at(0));
  Serial.println(dump_get_type_at(1));
  Serial.println(dump_get_type_at(2));
  Serial.println(dump_get_type_at(3));
  Serial.println(dump_get_type_at(4));
  Serial.println(dump_get_type_at(5));
  Serial.println(dump_get_type_at(6));
  Serial.println(dump_get_type_at(7));
  dump_eeprom();
#endif
}

void loop() { // this is a real mess :(
  if(eeprom_initialized_p() && run_animation_p){
    FastLED.delay(1000/FRAMES_PER_SECOND);
    led_update(dt);
    led_display();
    dt++;
  }
  if (Serial.available() > 0) {
    run_animation_p = FALSE;
    recv_command();
  }
}
