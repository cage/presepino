/*
  presepino: show LED patterns
  (c) 2018  cage

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.
  If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

 */

#define MAX_VOLT                       5 // Volt

#define MAX_CURRENT                 2000 // mA

#define PULSE_CHANGE_PATTERN_PERIOD  800

#define PULSE_CUTOFF                  25

CRGB    leds[LED_NUM];

uint8_t led_random[LED_NUM];

int     pulse_idx_offset = 0;

bool_t  pulse_fade_out_p = FALSE;

void led_setup() {
  FastLED.addLeds<WS2812B, LED_DATA_PIN, GRB>(leds, LED_NUM);
  FastLED.setCorrection(LED_CORRECTION);
  FastLED.setTemperature(LED_TEMPERATURE);
  FastLED.setDither(0); // avoid flickering workaround
  FastLED.setMaxPowerInVoltsAndMilliamps(MAX_VOLT,MAX_CURRENT);
  LEDS.setBrightness(LED_MAX_BRIGHTENSS);
  for (int i = 0; i< LED_NUM; i++){
    led_random[i] = random8();
  }
}

void led_set_hsv (int led_idx, uint8_t h, uint8_t s, uint8_t v) {
  leds[led_idx] = CHSV(h, s, v);
}

void led_set_hsv_range (int from, int to, uint8_t h, uint8_t s, uint8_t v) {
  for (int i = from; i < to; i++){
    led_set_hsv(i, h, s, v);
  }
}

void led_preview_hsv (uint8_t h, uint8_t s, uint8_t v) {
  unsigned char max = (LED_NUM / 4) > 0 ? (LED_NUM / 4) : 1;
  led_set_hsv_range(0,   max,     h, s, v);
  led_set_hsv_range(max, LED_NUM, 0, 0, 0);
}

void led_show(){
  FastLED.show();
}

int hour_to_minute (uint8_t m) {
  return m * 60;
}

uint8_t led_fading_lerp_weight (uint8_t from, uint8_t to, uint8_t hour, uint8_t minute) {
  int   minutes_diff = -1;
  int   minutes_now  = hour_to_minute(hour - min(from, to)) + minute;
  if(from > to){
    minutes_diff = hour_to_minute((24 - from) + to);
  } else {
    minutes_diff = hour_to_minute(to - from);
  }
  float w            = (float)minutes_now / (float)minutes_diff;
  return (uint8_t) (w * 256.0f);
}

uint8_t led_fading_lerp_weight (uint8_t from, uint8_t to) {
  return led_fading_lerp_weight(from, to, clock_get_hour(), clock_get_minute());
}


void led_update_fade (int from_idx, int to_idx, uint8_t from_h, uint8_t to_h) {
  uint8_t lerp_w = led_fading_lerp_weight(from_h, to_h);
  for(int i = 0; i < dump_read_led_num(); i++){
    uint8_t h1, s1, v1, h2, s2, v2;
    dump_get_color_at (from_idx, i, &h1, &s1, &v1);
    dump_get_color_at (to_idx,   i, &h2, &s2, &v2);
    h1 = lerp8by8 (h1, h2, lerp_w);
    s1 = lerp8by8 (s1, s2, lerp_w);
    v1 = lerp8by8 (v1, v2, lerp_w);
    led_set_hsv(i, h1, s1, v1);
  }
}

void led_fade_out (){
  uint8_t old = LEDS.getBrightness();
  if (old > 0) {
    old--;
  }
  LEDS.setBrightness(old);
}

void led_fade_in (){
  uint8_t new_brightness = min(LEDS.getBrightness() + 1, LED_MAX_BRIGHTENSS);
  LEDS.setBrightness(new_brightness);
}


void led_update_pulse (unsigned int dt, int from_idx, int to_idx) {
  if((dt % PULSE_CHANGE_PATTERN_PERIOD) == 0){
    pulse_fade_out_p = TRUE;
  }

  if (pulse_fade_out_p){
    if (LEDS.getBrightness() > 0){
      led_fade_out();
    } else {
      pulse_idx_offset++;
      pulse_fade_out_p = FALSE;
    }
  } else {
    led_fade_in(); // will saturate to LED_MAX_BRIGHTNESS
  }

  int idx_offset = (pulse_idx_offset % (to_idx - from_idx));
  uint8_t h, s, v;
  for(int i = 0; i < dump_read_led_num(); i++){
    dump_get_color_at (from_idx + idx_offset, i, &h, &s, &v);
    uint8_t p = v * sin8_C((uint8_t)dt + led_random[i]);
    if((v - p) < PULSE_CUTOFF){ // values too near to 0 gives poor results...
      p = v;
    }
    led_set_hsv(i, h , s, v - p);
  }
}

void led_update (unsigned int dt) {
  int from_idx = -1;
  int to_idx   = -1;
  dump_get_anim_limits(&from_idx, &to_idx);
  if(dump_type_at_fade_p(from_idx)){ // fading
    uint8_t from_h = dump_get_hour_at (from_idx);
    uint8_t to_h   = dump_get_hour_at (to_idx);
    led_update_fade(from_idx, to_idx, from_h, to_h);
  } else  {
    led_update_pulse(dt, from_idx, to_idx);
  }
}

void led_display() {
  led_show();
}
