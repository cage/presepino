/*
  presepino: show LED patterns
  (c) 2018  cage

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.
  If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

 */

bool_t eeprom_initialized_memoized = FALSE;

int rotate_right (int a, int shift, int max) {
  return (a + shift) % max;
}

int rotate_left (int a, int shift, int max) {
  int partial = a - abs(shift);
  if (partial < 0) {
    if (shift < max) {
      return max - shift;
    } else {
      return a - (abs(shift) - ((abs(shift) / max) * max));
    }
  } else {
    return partial;
  }
}

uint8_t eeprom_write(int address, int new_value){
  uint8_t old_value = EEPROM.read(address);
  if(old_value != new_value) {
#ifdef DEBUG
      Serial.println(F("."));
#endif
      EEPROM.write(address, new_value);
  }
  return old_value;
}

bool_t eeprom_initialized_p () {
  if (eeprom_initialized_memoized == TRUE){
    return TRUE;
  } else {
    if ((EEPROM.read(0) == VERSION_MAJOR) &&
        (EEPROM.read(1) == VERSION_MINOR) &&
        (EEPROM.read(2) == VERSION_REVISION)){
      eeprom_initialized_memoized = TRUE;
      return eeprom_initialized_p();
    } else {
      return FALSE;
    }
  }
}

void eeprom_initialize () {
  eeprom_write(0, VERSION_MAJOR);
  eeprom_write(1, VERSION_MINOR);
  eeprom_write(2, VERSION_REVISION);
}
